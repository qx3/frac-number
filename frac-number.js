/**
 *
 * var a = frac(1.234);     // 0.234
 * var b = frac(56789e-3);  // 0.789
 * var c = frac(12345678);  // 0
 * var d = frac(-34.5697);  // -0.5697
 * var e = frac('-.9');     // -0.9
 * var f = frac(null);      // 0
 * var g = frac(undefined); // NaN
 * var h = frac('sdfa');    // NaN
 *
 * @license Copyright 2014 - Chris West - MIT Licensed
 */
(function(RGX) {
    frac = function(num) {
        return +(+num).toExponential().replace(RGX, function(m, neg, num, dot, offset) {
            var zeroes = Array(Math.abs(offset) + 2).join('0');
            num = (zeroes + num + (dot ? '' : '.') + zeroes).split('.');
            return +(neg + '.' + num.join('').slice(+offset + num[0].length));
        });
    };
})(/(-?)(\d+(\.?)\d*)e(.+)/);
